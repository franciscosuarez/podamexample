package com.qabok.podam;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import uk.co.jemos.podam.api.DataProviderStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl; 

import com.qabok.podam.model.Country;
import com.qabok.podam.strategy.CountryDataProviderStrategy;

public class CountryCreatotWithStrategy {
	public static void main(String[] args) {
		/** Создаём нашу стратегию генерации */
		DataProviderStrategy strategy = new CountryDataProviderStrategy();
		/** Создаём фабрику на основании этой стратегии */
		PodamFactory factory = new PodamFactoryImpl(strategy);
		/** Генерим страну */
		Country myPojo = factory.manufacturePojo(Country.class);
		/** Печатаем страну */
		System.out.println(ReflectionToStringBuilder.toString(myPojo,new RecursiveToStringStyle()));	
	}
}
