package com.qabok.podam;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

import com.qabok.podam.model.Country;

public class CountryCreatorSimple  {
	public static void main(String[] args) {
		/** Создаём фабрику */
		PodamFactory factory = new PodamFactoryImpl();
		/** Генерим страну */
		Country myPojo = factory.manufacturePojo(Country.class);
		/** "Печатаем" страну */
		System.out.println(ReflectionToStringBuilder.toString(myPojo,new RecursiveToStringStyle()));
	}
}
