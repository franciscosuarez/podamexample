package com.qabok.podam.strategy;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import uk.co.jemos.podam.api.AbstractRandomDataProviderStrategy;
import uk.co.jemos.podam.api.AttributeMetadata;

import com.qabok.podam.model.City;
import com.qabok.podam.model.Country;
import com.qabok.podam.model.Street;

public class CountryDataProviderStrategy extends
		AbstractRandomDataProviderStrategy {
	private static final Random random = new Random(System.currentTimeMillis());

	public CountryDataProviderStrategy() {
		super();
	}

	@Override
	public String getStringValue(AttributeMetadata attributeMetadata) {
		/**
		 * Если поле name, то в зависимости от класса либо генерим улицу, либо
		 * город, либо страну
		 */
		if ("name".equals(attributeMetadata.getAttributeName())) {
			if (Street.class.equals(attributeMetadata.getPojoClass())) {
				return Streets.randomStreet();
			} else if (City.class.equals(attributeMetadata.getPojoClass())) {
				return Cities.randomCity();
			} else if (Country.class.equals(attributeMetadata.getPojoClass())) {
				return "Podam States of Mockitia";
			}
		}
		return super.getStringValue(attributeMetadata);
	};

	@Override
	public int getNumberOfCollectionElements(Class<?> type) {
		/**
		 * Если список городов, то вернём или 1 или 2. Если список улиц, то
		 * вернём от 1 до 10
		 */
		if (City.class.getName().equals(type.getName())) {
			return 1 + random.nextInt(2);
		} else if (Street.class.getName().equals(type.getName())) {
			return 1 + random.nextInt(10);
		}
		return super.getNumberOfCollectionElements(type);

	};

	@Override
	public Integer getInteger(AttributeMetadata attributeMetadata) {
		/** Ну и вернём разумное население */
		if (City.class.equals(attributeMetadata.getPojoClass())) {
			if ("population".equals(attributeMetadata.getAttributeName())) {
				return 1_000_000 + random.nextInt(9_000_000);
			}
		}
		return super.getInteger(attributeMetadata);
	}

	private enum Cities {
		MOSCOW, SAINT_PETERSBURG, LONDON, NEW_YORK, SHANGHAI, KARACHI, BEIJING, DELHI, PARIS, NAIROBI;

		private static final List<Cities> values = Collections.unmodifiableList(Arrays.asList(values()));
		private static final int size = values.size();
		private static final Random random = new Random();

		public static String randomCity() {
			return values.get(random.nextInt(size)).toString();
		}
	}

	private enum Streets {
		RUE_ABEL, RUE_AMPERE, AVENUE_PAUL_APPELL, BOULEVARD_ARAGO, JARDINS_ARAGO, SQUARE_ARAGO, RUE_ANTOINE_ARNAULD, SQUARE_ANTOINE_ARNAULD, RUE_BERNOULLI, RUE_BEZOUT, RUE_BIOT, RUE_BORDA, SQUARE_BOREL, RUE_CHARLES_BOSSUT, RUE_DE_BROGLIE, RUE_BUFFON, AVENUE_CARNOT, BOULEVARD_CARNOT, VILLA_SADI_CARNOT, RUE_CASSINI, RUE_CAUCHY, RUE_MICHEL_CHASLES, RUE_NICOLAS_CHUQUET, RUE_CLAIRAUT, RUE_CLAPEYRON, RUE_CONDORCET, RUE_CORIOLIS, RUE_COURNOT, RUE_GASTON_DARBOUX, RUE_DELAMBRE, SQUARE_DELAMBRE, RUE_DEPARCIEUX, RUE_DE_PRONY, RUE_DESARGUES, RUE_DESCARTES, RUE_ESCLANGON, RUE_EULER;

		private static final List<Streets> values = Collections.unmodifiableList(Arrays.asList(values()));
		private static final int size = values.size();
		private static final Random random = new Random();

		public static String randomStreet() {
			return values.get(random.nextInt(size)).toString();
		}
	}
}
