package com.qabok.podam.model;

import java.util.ArrayList;
import java.util.List;

public class Country {
	private String name;
	private Currency currency;
	private List<City> cities;	
	
	public Country() {		
		setCities(new ArrayList<City>());
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
