package com.qabok.podam.model;

public enum Currency {
	RUB,
	EUR,
	USD;
}
